# Репозиторий курса Алгоритмы и структуры данных ВФТШ #
В папке **[seminars](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/seminars/)** будут выкладываться материалы с занятий  
В папке **[home_tasks](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/home_tasks/)** будут выкладываться домашние задания  
**[C++ style guide или как писать код:](https://bitbucket.org/Pelmeshek/vftsh.algo.2019/src/master/style_guide.md)**  

**Полезные ссылки**:  
[Миникурс по основам с++](https://stepik.org/course/16480/syllabus)  
[Изучение git в игровой форме](https://learngitbranching.js.org/). Здесь задачи могут попасьтся сложнее требуемого в вфтш уровня, но штука интересная  