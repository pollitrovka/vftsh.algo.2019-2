#include <iostream>

struct TStackEl {

    int Data;
    TStackEl* Next;
};

class TStack {
    unsigned int Len;
    TStackEl* Head;
public:
    Stack(): Len(0) {}

    void Pop() {

    }

    int Top() {
        return Head->Data;
    }

    void Push(int x) {
        TStackEl* newEl = new TStackEl;
        newEl->Data = x;
        newEl->Next = Head;
        Head = newEl;
        Len++;
    }

    bool Empty() {
        return Len == 0;
    }

    unsigned int Size() {
        return Len;
    }
};

